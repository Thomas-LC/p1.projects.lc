<?php

ini_set('display_errors', 1);
ini_set('html_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);



if (!defined('_PS'))
    define('_PS', '/');
spl_autoload_register(function ($class) {
    $path = Array(
        _PS.'class'._PS ,
        _PS.'libs'._PS ,
        _PS.'view'._PS
    );
    foreach ($path as $files) {
        $fileClass = __DIR__ . $files . $class . '.php';

        if (file_exists($fileClass))
            require_once($fileClass);
    }
});

//DEFINICJA SCIEŻEK DOSTĘPU
$AbsoluteURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
$AbsoluteURL .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
$slash = substr($AbsoluteURL, -1);
$NewURL = $slash != '/' ? $AbsoluteURL . '/' : $AbsoluteURL;

define('SERVER_ADDRESS', $NewURL );
define('_ROOT', '.'._PS );
define('TEMPLATES', _ROOT.'system'._PS.'templates' );
define('TEMPLATESC', _ROOT.'system'._PS.'templates_c'._PS );
define('CACHE', _ROOT.'system'._PS.'cache'._PS );
define('CONFIGS', _ROOT.'system'._PS.'configs'._PS );


# LOAD MODULES CLASS #
$_DBMANAGER = S::register("DBdriver");
$_MEMCACHED = S::register("CacheData");
$_MODULES = S::register("Modules");
$_CATEGORIES = S::register("Categories");
$_T = S::register("Template");

#LOAD VIEW CLASS #
$_VCAT = S::register("CategoriesView");
$_VPRC = S::register("ProducersView");


function pr($val, $exit = FALSE) {
    echo'<pre>';
    print_r($val);
    echo'</pre>';
    if ($exit)
        exit();
}
