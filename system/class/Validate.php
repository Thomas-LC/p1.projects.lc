<?php

/**
 * Description of Validate
 *
 * @author thomas
 */
class Validate {

    private $_DBMANAGER;
    
    public function __construct() {
        $this->_DBMANAGER = S::register("DBdriver");
    }

    public function _GET($param) {
        return $this->getParam($param);
    }

    public function _POST($param) {
        return $this->getParam($param);
    }

    /*
     * KAŻDY GET I POST MUSI TĘDY
     * 
     * ************************************************************* */

    private function getParam($param) {

        foreach ($param as $key => $val) {
            $post[$this->mysqlFixStringEntites($key)] = $this->mysqlFixStringEntites($val);
        }
        return $post;
    }

    /*     * ************************************************************ */


    private function mysqlFixStringEntites($string) {
        $mysqlFixString = $this->mysqlFixString($string);
        return htmlentities($mysqlFixString);
    }

    private function mysqlFixString($string) {
        if (get_magic_quotes_gpc())
            $string = stripslashes($string);
        return $string;
    }

}
