<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Producers
 *
 * @author Thomas Ross
 */
class Producers extends Modules{

    private $_loadPage = [];
    private $_modId = 4;
    
    public function __construct() {
        parent::__construct();

        $this->_loadPage = $this->_urls;

    }
    
    protected function loadProducers() {

        return $this->_loadPage;

    }
    protected function loadProducersUrl() {

        return $this->getUriFromModules($this->_modId) ;

    }
    protected function getProducersTree(){
        return $this->getModulesList('producers');
   
    }
}
