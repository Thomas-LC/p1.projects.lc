<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Articles
 *
 * @author thomas
 */
class Articles extends Modules {
    
    private $_loadPage = [];
    
    public function __construct() {
        parent::__construct();

        $this->_loadPage = $this->_urls;

    }
    public function loadArticles() {

        try{
            return $this->_loadPage;
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }

    }

}
