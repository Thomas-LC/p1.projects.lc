<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Modules
 *
 * @author Thomas Ross
 */

class Modules extends Router {

    protected $_urls;
    private $_url;
    private $_content;

    public function __construct() {
        parent::__construct();
        
        foreach($this->getUriFromGet() as $value){

            $this->_url = $value->module_name;
            $this->_urls = $value;
        }
       
    }

    public function loadModulesContent() {
        $_MODULES = S::register(ucfirst($this->_url));
        
        switch ($this->_url) {
            case 'articles':
                $this->_content = $_MODULES->loadArticles();
                break;
            case 'products':
                $this->_content = $_MODULES->loadProducts();
                break;
            case 'producers':
                $this->_content = $_MODULES->loadProducers();
                break;
            case 'categories':
                $this->_content = $_MODULES->loadCategories();
                break;
            default :
                $this->loadErrorPage();
                break;
        }        
        return $this->_MEMCACHED->getCacheData($this->_content);
    }
    
    protected function getModulesList($modulesTableName){
        return $this->_DBMANAGER::dbAll("SELECT * FROM $modulesTableName");
    }

    
}
