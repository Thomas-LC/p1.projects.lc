<?php

/**
 * Description of DBdriver
 *
 * @author thomas
 */
class DBdriver {

    private static function getConPDO() {

        $_C = S::register("Config");
        $DSN = "mysql:host={$_C->db_host};dbname={$_C->db_name};charset={$_C->db_char}";
        try {
            $DBS = new PDO($DSN, $_C->db_user, $_C->db_pass);
            $DBS->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $DBS->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            return $DBS;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public static function dbOne($statement, $input_parameters = Array()) {
        try {
            $sth = static::getConPDO()->prepare($statement);
            $sth->execute($input_parameters);
            return $sth->fetch();
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public static function dbAll($statement, $input_parameters = FALSE) {
        //$input_parameters = Array();
        
//        pr($input_parameters);
//        pr($statement,1);

        try {
            $sth = static::getConPDO()->prepare($statement);
            $input_parameters ? $sth->execute($input_parameters) : $sth->execute();
            
//            pr($input_parameters);
//            pr($sth);
            return $sth->fetchAll();
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public static function dbAct($statement, $input_parameters = Array()) {
        try {
            $sth = static::getConPDO()->prepare($statement);
            $sth->execute($input_parameters);
            return TRUE;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public static function dbCount($statement, $input_parameters = Array()) {
        try {
            $sth = static::getConPDO()->prepare($statement);
            $sth->execute($input_parameters);
            return $sth->rowCount();
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public static function dbQuote($string) {
        return static::getConPDO()->quote($string);
    }

    public function __destruct() {
        try {
            $Conn = static::getConPDO();
            $Conn = NULL;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

}
