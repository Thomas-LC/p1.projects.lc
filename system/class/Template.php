<?php

/**
 * Description of Template
 *
 * @author thomas
 */
class Template {

    public $Smarty, $Fileconf;
    public function __construct() {
        $this->Smarty = S::register("Smarty");
         $this->Smarty->setTemplateDir(TEMPLATES);
	 $this->Smarty->setCompileDir(TEMPLATESC);
	 $this->Smarty->setCacheDir(CACHE);
	 $this->Smarty->setConfigDir(CONFIGS);
         $this->Smarty->caching = 2; // lifetime is per cache        
         $this->Smarty->cache_lifetime = 1;// set the cache_lifetime for index.tpl to 5 minutes
    }   

    public function loadAssign($name, $value){
        
        
        $loadAssign = $this->Smarty->assign($name, $value);
        return $loadAssign;
    }
    
    public function loadTemplate($file){
        $loadTemplate = $this->Smarty->display($file);
        return $loadTemplate;
    }
    
    public function loadFetch($file){
        $loadTemplate = $this->Smarty->fetch($file);
        return $loadTemplate;
    }

}
