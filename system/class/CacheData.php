<?php

/**
 * Description of CacheData
 *
 * @author thomas
 */
class CacheData {

    const _TTL = 1; //Second

    private $KEYCACHED, $MEMCACHED, $CONFIGS, $STATUSSERVER;
    private $ADATA = [];

    public function __construct() {

        $this->CONFIGS = S::register("Config");
        $this->MEMCACHED = S::register("Memcached");
        $this->MEMCACHED->addServer($this->CONFIGS->cacheServer, $this->CONFIGS->cacheServerPort) or die("Could not connect");
        $this->KEYCACHED = $this->CONFIGS->cacheKey;
        $this->STATUSSERVER = $this->MEMCACHED->getStats();
    }

    private function generateKey($data) {
        return md5($this->KEYCACHED . $data->desc);
    }

    private function generateData($data) {
        return $this->MEMCACHED->get($this->generateKey($data));
    }

    public function getCacheData($data) {
        try {
            if ($this->generateData($data)) {
                $assocData = $this->generateData($data);
                pr($this->generateKey($data));
            } else {
                $assocData = $data;
                $this->MEMCACHED->set($this->generateKey($data), $assocData, self::_TTL);
                pr($this->generateKey($data));
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
        return $this->ADATA = $assocData;
    }

}
