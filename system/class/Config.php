<?php

/**
 * Description of Config
 *
 * @author thomas
 */
class Config {

    protected $Config;
    private $File =CONFIGS.'configs.php';

    public function __construct() {

        file_exists($this->File) ? include_once($this->File) : include_once('../system/configs/configs.php');
        isset($configs) ? $this->Config = $configs : $this->Config = '';
    }

    public function __get($val) {
        return $this->Config[$val];
    }

}
