<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Router
 *
 * @author thomas
 */
class Router {

    const _DEFAULT_MOD = 'articles';

    private $_uri = [];
    private $_mod = [];
    private $_get = [];
    private $_modId = [];
    protected $_QloadData = [];
    protected $_DBMANAGER;
    protected $_VALIDATE;
    protected $_MEMCACHED;

    public function __construct() {

        $this->_DBMANAGER = S::register("DBdriver");
        $this->_VALIDATE = S::register("Validate");
        $this->_MEMCACHED = S::register("CacheData");

        $this->_get = $this->_VALIDATE->_GET($_GET);
        $this->_QloadData[] = $this->setLoadData();
    }

    private function setUri($uri) {
        $this->_uri[] = $uri;
    }

    private function setMod($mod) {
        $this->_mod[] = $mod;
    }

    private function setModId($modId) {
        $this->_modId[] = $modId;
    }

    private function setLoadData() {

        $mod = $this->_get['mod'];
        $lang = $this->_get['lang'];
        
        

        empty($mod) ? $mod = self::_DEFAULT_MOD : $mod;

        if ($this->_DBMANAGER::dbCount("SELECT id FROM modules WHERE name = ?", [$mod]) > 0) {

            try {
                $this->_QloadData = $this->getQuery($mod, $lang);
            } catch (Exception $ex) {
                throw new Exception($ex->getMessage());
            }
        } else {

            $this->loadErrorPage();
        }
    }

    private function getQuery($mod, $getLang = false) {
        $id = $this->getIdMod($mod);
        $idLang = $this->getIdLang($getLang);
        $modOb = new stdClass();

        if (strlen($getLang) == 2) {
            $modOb->sufix = '_lang';
            $modOb->mod = $mod . $modOb->sufix;
            $modOb->and = ' AND u_lang.lang_id=' . $modOb->mod . '.lang_id AND u_lang.lang_id = ?';
            $modOb->idLangArr = [$idLang->id, $id->id];
            $modOb->uLangId = 'u_lang.lang_id, ';
        } else {
            $modOb->sufix = '';
            $modOb->mod = $mod;
            $modOb->and = '';
            $modOb->idLangArr = [$id->id];
            $modOb->uLangId = '';
        }
        return $this->_DBMANAGER::dbAll("SELECT u" . $modOb->sufix . ".url, u" . $modOb->sufix . ".mod_id, u" . $modOb->sufix . ".mod_key, " . $modOb->uLangId . ""
                        . "$modOb->mod.id, $modOb->mod.name , $modOb->mod.desc, "
                        . "m.id AS module_id, m.name AS module_name FROM url" . $modOb->sufix . " AS u" . $modOb->sufix . ", $modOb->mod, modules AS m "
                        . "WHERE u" . $modOb->sufix . ".mod_key=$modOb->mod.id "
                        . "AND u" . $modOb->sufix . ".mod_id=m.id"
                        . $modOb->and . " " //AND 
                        . "AND u" . $modOb->sufix . ".mod_id = ?", $modOb->idLangArr);
    }

    private function getIdMod($mod) {
        return $this->_DBMANAGER::dbOne("SELECT id FROM modules WHERE name = ?", [$mod]);
    }

    private function getIdLang($getLang) {
        return $this->_DBMANAGER::dbOne("SELECT id FROM lang WHERE short_name = ?", [$getLang]);
    }

    protected function loadErrorPage() {
        header("Location: /404.html");
    }

    protected function getUriFromGet($all = false) {

        foreach ($this->_QloadData as $value) {

            $this->_uri[] = $this->setUri($value->url);
            $this->_mod[] = $this->setMod($value->module_name);
            $this->_modId[] = $this->setModId($value->mod_id);
        }

        $uriGet = isset($this->_get['page']) ? $this->_get['page'] : '/';
        $loadMod = isset($this->_get['page']) ? '' : self::_DEFAULT_MOD;
        $modGet = isset($this->_get['mod']) ? $this->_get['mod'] : $loadMod;

        if (in_array($uriGet, $this->_uri) && in_array($modGet, $this->_mod)) {

            if (!$all) {
                foreach ($this->_QloadData as $value) {
                    if ($value->url === $uriGet) {
                        $val[] = $value;
                    }
                }
                return $val;
            } else
                return $this->_QloadData;
        } else
            return $this->loadErrorPage();
    }
    
    protected function getUriFromModules($mod) {

        if (strlen($this->_get['lang']) == 2){
            $langSufix = 'lang_';
        }else{
            $langSufix = '';
        }
        
        return $this->_DBMANAGER::dbAll("SELECT url, mod_key FROM ".$langSufix."url WHERE mod_id = ?", [$mod]);

    }

}
