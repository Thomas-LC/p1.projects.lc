<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products
 *
 * @author Thomas Ross
 */
class Products extends Modules {

    private $_loadPage = [];
    
    public function __construct() {
        parent::__construct();

        $this->_loadPage = $this->_urls;

    }
    public function loadProducts() {
        
       return $this->_loadPage;
    }
    
}
