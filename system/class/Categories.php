<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Producers
 *
 * @author Thomas Ross
 */
class Categories extends Modules{

    private $_loadPage = [];
    private $_modId = 2;
    
    public function __construct() {
        parent::__construct();

        $this->_loadPage = $this->_urls;

    }

    protected function loadCategories() {

        return $this->_loadPage;
    }

    protected function loadCategoriesUrl() {

        return $this->getUriFromModules($this->_modId) ;

    }
    
    protected function getCategoriesTree(){
        return $categories = $this->getModulesList('categories');
   
    }
    
    
    
}
