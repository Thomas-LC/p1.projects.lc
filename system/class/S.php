<?php

/**
 * Description of Singleton
 *
 * @author thomas
 */
class S {

    private static $registered = Array();

    public static function register($object, $params = FALSE) {
        if (empty(static::$registered[$object])) {
            !$params ? static::$registered[$object] = new $object() : static::$registered[$object] = new $object($params);
        }
        return static::$registered[$object];
    }

}
