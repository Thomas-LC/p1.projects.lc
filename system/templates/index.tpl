{config_load file="test.conf" section="setup"}
{include file="header.tpl" title=$content->name name=$content->name}

<div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar navbar-header">
            <a href="{$base}" class="navbar-brand">MYSHOP</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{$base}"><span class="glyphicon glyphicon-home"></span> Home</a> </li>
            <li><a href="#"><span class="glyphicon glyphicon-modal-window"></span> Products</a> </li>
            <li style="width:300px;left:10px;top:10px;"><input type="text" class="form-control" id="search"></li>
            <li style="top:10px;left:20px;"><button class="btn btn-primary" id="search_btn" >Search</button></li>
        </ul>
    
        <ul class="nav navbar-nav navbar-right">
        <li><a href="#" id="cart_container" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span> Cart <span class="badge">0</span> </a>
            <div class="dropdown-menu" style="width: 400px;">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-3">S1.No</div>
                            <div class="col-md-3">PRImg</div>
                            <div class="col-md-3">Pro Name</div>
                            <div class="col-md-3">Price in $</div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="cart_product"></div>
                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
        </li>
        <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> SignIn</a>
            <ul class="dropdown-menu">
                <div style="width: 300px;">
                    <div class="panel panel-primary">
                    <div class="panel-heading">Login</div>
                        <div class="panel-heading">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" required />
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" required />
                            <p><br></p>
                            <a href="#" style="color: white; list-style: none;">Forgotten Password</a>
                            <input type="submit" class="btn btn-success" style="float: right;" id="login" value="Login" />
                        </div>
                        <div class="panel-footer" id="e_msg"></div>
                    </div>
                </div>
            </ul>
        </li>
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> SignUp</a> </li>
    </ul>
  </div>

    <p><br></p>
    <p><br></p>
    <p><br></p>
    
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div id="get_category">
                    
                    
                </div>
                <div id="get_producers"></div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading"></div>
                    <div class="panel-body">

                    <div id="get_products"></div>
                        {$content->desc}
                    </div>
                    <div class="panel-footer">&copy; 2017</div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    


{include file="footer.tpl"}
