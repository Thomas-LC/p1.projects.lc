<!DOCTYPE html>
<html lang="{$lang}">
<head>
  <base href="{$base}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta charset="utf-8">
  <title>{$title}</title>
  <script></script>

    <meta name="locale" content="{$locale}">

    <meta name="application-name" content="{$title}">
    <meta name="apple-mobile-web-app-title" content="{$title}">

    <meta name="description" content="{$desc}">
  
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="mobile/jquery-mobile.css" />
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/main.js"></script>
    <script src="mobile/jquery-mobile.js"></script>

</head>

<body data-role="page">