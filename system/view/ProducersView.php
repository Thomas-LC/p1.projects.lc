<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoriesView
 *
 * @author thomas
 */
class ProducersView extends Producers {
    public function __construct() {
        parent::__construct();
    }
    
    public function loadProducersTree(){
        if(isset($_POST['producers'])){            
            $tree =  '<div class="nav nav-pills nav-stacked">'
                    . '<li class="active"><a href="#"><h4>Producers</h4></a> </li>';
            
            foreach( $this->loadProducersUrl() as $key=>$url ){
                $urls[$url->mod_key] = $url->url; 
            }
            
            foreach($this->getProducersTree() as $prc){
                $tree .= '<li><a href="producers/'.$urls[$prc->id].'" cid="'. $prc->id .'" class="category">'. $prc->name .'</a> </li>';
            }
                    
            $tree .= '</div>';        
        }else
            $tree = '';
        
        echo $tree;
    }
    

}
