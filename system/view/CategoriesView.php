<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoriesView
 *
 * @author thomas
 */
class CategoriesView extends Categories {
    public function __construct() {
        parent::__construct();
    }
    
    public function loadCategoriesTree(){
        if(isset($_POST['category'])){            
            $tree =  '<div class="nav nav-pills nav-stacked">'
                    . '<li class="active"><a href="#"><h4>Categories</h4></a> </li>';
            
            foreach( $this->loadCategoriesUrl() as $key=>$url ){
                $urls[$url->mod_key] = $url->url; 
            }
            
            foreach($this->getCategoriesTree() as $cat){
                $tree .= '<li><a href="categories/'.$urls[$cat->id].'" cid="'. $cat->id .'" class="category">'. $cat->name .'</a> </li>';
            }
                    
            $tree .= '</div>';        
        }else
            $tree = '';
        
        echo $tree;
    }
    

}
